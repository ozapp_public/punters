export const state = () => ({
  weathers: [],
  countryId: "",
  countries: [],
});

export const mutations = {
  updateWeathers: (state, weathers) => {
    for (let index = 0; index < weathers.length; index++) {
      const weather = weathers[index];
      weather._weatherTemp = weather._weatherTemp ? parseInt(weather._weatherTemp) : 0;
    }
    state.weathers = weathers;
  },
  updateCountries: (state, weathers) => {
    var countries = [], countryIds = [];
    for (let index = 0; index < weathers.length; index++) {
      const weather = weathers[index];
      const _country = weather._country;

      if(countryIds.indexOf(_country._countryID) === -1){
        countryIds.push(_country._countryID);
        countries.push({..._country})
      }
    }
    state.countries = countries;
  },
  updateCurrentCountry: (state, country) => {
    state.countryId = country._countryID;
  }
};

export const actions = {
  async getWeathers({ commit }) {
    const endpoint = '/venues-weather.json';
    const res = await this.$axios.$get(endpoint);
    if (res.isOkay){
      commit('updateWeathers', res.data);
      commit('updateCountries', res.data);
    } 
  },
  setCountryId({ commit }, country) {
    commit('updateCurrentCountry', country);
  },
};