# Punters

Using Vue.js & NuxtJS frameworks develop a single page web app that fetches and displays the weather conditions for different suburbs around Australia and some other countries. Some sorting/filtering of the weather data will also be involved.

## Docker Setup

One of the quickest ways to get up and running on your machine is by using Docker:
```
docker run --name punters-ozapp-mobi -v $(pwd):/src/app \
           -w="/src/app" -it -p 3000:3000 node:14 bash
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start
```

For detailed explanation on how NuxtJs work, check out [Nuxt.js docs](https://nuxtjs.org).

